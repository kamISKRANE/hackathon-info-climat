import {
  Webcam,
  Event,
  DidYouKnow,
  MapInteractive,
  DailyNews,
  OnThisDay,
  SocialNetwork,
} from '../dashboard/widgets';

export const widgetGridConfig = [
  {
    minW: 2,
    minH: 2,
    maxW: 12,
    maxH: 8,
    i: 'Webcam',
    x: 0,
    y: 0,
    w: 7,
    h: 7,
    label: 'Webcam',
  },
  {
    minW: 3,
    minH: 1,
    maxW: 6,
    maxH: 1,
    i: 'Event',
    x: 7,
    y: 0,
    w: 3,
    h: 1,

    label: 'Événements',
  },
  {
    minW: 4,
    minH: 2,
    maxW: 12,
    maxH: 2,
    i: 'DidYouKnow',
    x: 7,
    y: 1,
    w: 5,
    h: 2,

    label: 'Le saviez vous',
  },
  {
    minW: 4,
    minH: 2,
    maxW: 12,
    maxH: 3,
    i: 'MapInteractive',
    x: 0,
    y: 2,
    w: 4,
    h: 2,

    label: 'Map interactive',
  },
  {
    minW: 2,
    minH: 3,
    maxW: 5,
    maxH: 6,
    i: 'DailyNews',
    x: 7,
    y: 2,
    w: 2,
    h: 3,

    label: 'Info du jour',
  },
  {
    minW: 4,
    minH: 2,
    maxW: 12,
    maxH: 2,
    i: 'OnThisDay',
    x: 0,
    y: 3,
    w: 6,
    h: 2,
    label: 'Ce jour là',
  },
  {
    minW: 5,
    minH: 4,
    maxW: 8,
    maxH: 5,
    i: 'SocialNetwork',
    x: 0,
    y: 5,
    w: 5,
    h: 6,

    label: 'Nos Réseaux sociaux',
  },
];

export const getComponent = (type) => {
  switch (type) {
    case 'Webcam':
      return Webcam;

    case 'Event':
      return Event;

    case 'DidYouKnow':
      return DidYouKnow;

    case 'MapInteractive':
      return MapInteractive;

    case 'DailyNews':
      return DailyNews;

    case 'OnThisDay':
      return OnThisDay;

    case 'SocialNetwork':
      return SocialNetwork;

    default:
      return null;
  }
};
