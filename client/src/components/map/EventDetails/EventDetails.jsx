/* eslint-disable react/no-unescaped-entities */
import { Box, Button, makeStyles, SwipeableDrawer } from '@material-ui/core';
import React from 'react';
import wording from 'src/utils/wording.json';

import AddIcon from '@material-ui/icons/Add';

const useStyles = makeStyles((theme) => ({
  drawer: {
    fontWeight: 600,
    margin: '10px 0px',
    fontSize: '20px',

    '& .MuiBackdrop-root': {
      backgroundColor: 'transparent',
    },
  },
  paper: {
    height: '93%',
    marginTop: '4.4%',
    width: '410px',
  },

  title: {
    padding: '20px 70px',
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: theme.palette.secondary.main,
    marginRight: 50,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,

    '& > p': {
      color: '#fff',
      fontSize: 30,
    },
  },
}));

const EventDetails = ({ data, handleToggle }) => {
  const classes = useStyles();

  if (!data) {
    return null;
  }

  const typeObj = wording.eventTypes.find(
    (e) => e.value === parseInt(data.type, 10)
  );

  return (
    <SwipeableDrawer
      anchor="right"
      open={data !== undefined}
      onClose={handleToggle}
      onOpen={handleToggle}
      className={classes.drawer}
      classes={{ paper: classes.paper }}
    >
      <Box>
        <Box className={classes.title} mt={10}>
          <p>{typeObj ? typeObj.label : ''}</p>
          <p>{data.nom}</p>
        </Box>

        <Box p={5}>
          <p>
            La vague de chaleur qui a sévi du 15 au 20 a contribué à faire
            d'août 2009 un mois particulièrement chaud. Avec une anomalie
            mensuelle globale de +1,7 °C, il se classe ainsi au 4ème rang des
            mois d'août les plus chauds depuis 1950. (Source : Météo-France)
          </p>

          <Box mt={6}>
            <Button
              startIcon={<AddIcon />}
              color="secondary"
              variant="outlined"
            >
              EN SAVOIR PLUS
            </Button>
          </Box>
        </Box>
      </Box>
    </SwipeableDrawer>
  );
};
export default EventDetails;
