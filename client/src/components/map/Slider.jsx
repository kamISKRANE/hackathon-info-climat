import React from 'react';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import PlayCircleFilledIcon from '@material-ui/icons/PlayCircleFilled';
import { useDispatch, useSelector } from '../../store';
import { filterEvents, resetHistoricEventsData } from '../../slices/map';

const useStyles = makeStyles({
  slider: {
    zIndex: 9999,
    '& .MuiSlider-markLabel[data-index="0"]': {
      left: '7% !important;'
    },
    '& .MuiSlider-markLabel[data-index="1"]': {
      left: '93% !important;'
    },
    '& .MuiSlider-thumb[data-index="1"]': {
      backgroundColor: '#FFFFFF',
      width: '20px',
      borderRadius: '20px',
      border: '1px solid black',
    }
  },
  sliderLabel: {
    marginTop: -30
  },
  playButton: {
    padding: 0
  },
});

export default function RangeSlider(props) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { slider, updateDatePickerBySlider, dateArray } = props;
  const [value, setValue] = React.useState([slider.valueMin, 5, slider.valueMax]);
  const { historicEventsToFilter } = useSelector((state) => state.map);
  const handleChange = (event, newValue) => {
    setValue(newValue);
    updateDatePickerBySlider(newValue);
  };

  const maxRun = value[2];
  let stopTimeLaspe = null;
  let i = value[0];

  const handleFilterEvents = () => {
    dispatch(filterEvents({ params: historicEventsToFilter, dateArray: dateArray[i] }));
  };

  const handleResetHistoricEvents = () => {
    dispatch(resetHistoricEventsData());
  };

  const doTimeLaspe = () => {
    if (i === maxRun) {
      clearInterval(stopTimeLaspe);
    }

    handleFilterEvents();

    i += 1;
    setValue([value[0], i, value[2]]);
  };

  const runTimeLaspe = () => {
    handleResetHistoricEvents();
    // eslint-disable-next-line prefer-destructuring
    i = value[0];
    stopTimeLaspe = setInterval(() => {
      doTimeLaspe();
    }, 150);
  };

  return (
    <Box>
      <Slider
        value={value}
        onChange={handleChange}
        valueLabelDisplay="auto"
        aria-labelledby="range-slider"
        step={1}
        // min={slider.min}
        // max={slider.max}
        marks={slider.marks}
        className={classes.slider}
      />
      <Typography align="center" id="range-slider" className={classes.sliderLabel}>
        <IconButton color="primary" aria-label="upload picture" component="span" className={classes.playButton} onClick={runTimeLaspe}>
          <PlayCircleFilledIcon style={{ fontSize: 30 }} />
        </IconButton>
      </Typography>
    </Box>
  );
}
