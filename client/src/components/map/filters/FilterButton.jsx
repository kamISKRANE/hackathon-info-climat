import React, { useRef, useState } from 'react';

import {
  Box,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  makeStyles,
  Popover,
  SvgIcon,
  Typography,
} from '@material-ui/core';

import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';

const useStyles = makeStyles({
  btn_container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '10px 0ox',
    cursor: 'pointer',
  },

  checkbox_form: {
    padding: '10px 40px',
  },

  checkbox_item: {
    // padding: '15px 0px',
  },

  title: {},
});

const FilterButton = ({ title, stateValues, items, handleChange }) => {
  const classes = useStyles();
  const ref = useRef(null);

  const [isOpen, setOpen] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <React.Fragment>
      <Box
        ref={ref}
        className={classes.btn_container}
        onClick={handleOpen}
        pt={1}
        pb={1}
      >
        <Typography component="h4" variant="h4" className={classes.title}>
          {title}
        </Typography>

        <SvgIcon fontSize="large">
          {isOpen ? <ExpandLessIcon /> : <ExpandMoreIcon />}
        </SvgIcon>
      </Box>

      <Popover
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        classes={{ paper: classes.popover }}
        anchorEl={ref.current}
        onClose={handleClose}
        open={isOpen}
      >
        <FormControl component="fieldset" className={classes.formControl}>
          <FormGroup className={classes.checkbox_form}>
            {items.map((obj, idx) => (
              <FormControlLabel
                key={idx}
                className={classes.checkbox_item}
                control={
                  <Checkbox
                    checked={stateValues.includes(obj.value)}
                    onChange={() => handleChange(obj)}
                    name="gilad"
                    size="medium"
                  />
                }
                label={obj.label}
              />
            ))}
          </FormGroup>
        </FormControl>
      </Popover>
    </React.Fragment>
  );
};

export default FilterButton;
