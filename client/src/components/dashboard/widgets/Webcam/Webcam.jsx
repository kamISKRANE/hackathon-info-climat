import { makeStyles, MenuItem } from '@material-ui/core';
import React from 'react';

import WidgetContainer from '../../WidgetContainer';

const useStyles = makeStyles(() => ({
  img: {
    // objectFit: 'cover',
    flexShrink: 0,
    width: '100%',
    height: 'auto',
  },
}));

const Webcam = () => {
  const classes = useStyles();

  const renderMenuContent = () => (
    <MenuItem>
      <div>
        <p>Test</p>
      </div>
    </MenuItem>
  );

  return (
    <WidgetContainer
      title="Webcam"
      menuType="custom"
      MenuContent={renderMenuContent()}
    >
      <img className={classes.img} alt="webcam" src="/static/webcam.jpg" />
    </WidgetContainer>
  );
};

export default Webcam;
