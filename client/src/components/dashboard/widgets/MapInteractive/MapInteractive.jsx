import { Box, IconButton, makeStyles, SvgIcon } from '@material-ui/core';
import React from 'react';
import { MapContainer, TileLayer } from 'react-leaflet';

import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';

import { useHistory } from 'react-router-dom';
import WidgetContainer from '../../WidgetContainer';

const useStyles = makeStyles((theme) => ({
  container: {
    position: 'relative',
  },

  btn: {
    position: 'absolute',
    bottom: 20,
    right: 20,
    zIndex: 1000,
    color: '#fff',
    backgroundColor: theme.palette.secondary.main,
    '&:hover': {
      color: theme.palette.secondary.main,
    },
  },
}));

const MapInteractive = () => {
  const classes = useStyles();
  const history = useHistory();

  const handleRedirect = () => {
    history.push('/historic');
  };
  return (
    <WidgetContainer title="Map interactive">
      <Box className={classes.container}>
        <IconButton
          size="medium"
          className={classes.btn}
          onClick={handleRedirect}
        >
          <SvgIcon>
            <ArrowForwardIosIcon />
          </SvgIcon>
        </IconButton>

        <MapContainer
          style={{ height: '250px ', width: '100%' }}
          center={[47, 2]}
          zoom={6}
          scrollWheelZoom={false}
        >
          <TileLayer
            attribution='&copy; <a href="http://osm.org/copyright"></a>'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
        </MapContainer>
      </Box>
    </WidgetContainer>
  );
};

export default MapInteractive;
