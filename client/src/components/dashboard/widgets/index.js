import Webcam from './Webcam';
import Event from './Event';
import DidYouKnow from './DidYouKnow';
import MapInteractive from './MapInteractive';
import DailyNews from './DailyNews';
import OnThisDay from './OnThisDay';
import SocialNetwork from './SocialNetwork';

export { Webcam, Event, DidYouKnow, MapInteractive, OnThisDay, DailyNews, SocialNetwork };
