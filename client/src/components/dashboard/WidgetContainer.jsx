import {
  Box,
  Card,
  CircularProgress,
  Divider,
  makeStyles,
  Typography,
} from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import clsx from 'clsx';
import React from 'react';

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    // marginBottom: theme.spacing(3),
  },

  card: {
    padding: 0,
    boxShadow: theme.shadows[30],
    borderRadius: '10px',
    position: 'relative',
  },

  title: {
    color: theme.palette.text.primary,
    fontSize: 20,
    fontWeight: 600,
  },

  customDivider: {
    width: '60px',
    marginBottom: theme.spacing(1),
    backgroundColor: theme.palette.primary.main,
    height: '4px',
  },

  menu_standard: {
    position: 'absolute',
    right: 0,
  },
  menu_custom: {
    position: 'absolute',
    right: 0,
    color: theme.palette.primary.main,
  },
  padding: {
    padding: '5px 20px',
  },
}));

const WidgetContainer = ({
  title,
  children,
  cardStyle = {},
  menuType = 'standard',
  MenuContent,
  padding,
  loading,
}) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const renderMenu = () => (
    <React.Fragment>
      <IconButton
        aria-label="more"
        aria-controls="long-menu"
        aria-haspopup="true"
        onClick={handleClick}
        color="secondary"
        className={classes[`menu_${menuType}`]}
      >
        <MoreVertIcon fontSize="large" />
      </IconButton>
      <Menu
        id="long-menu"
        anchorEl={anchorEl}
        keepMounted
        open={open}
        onClose={handleClose}
        PaperProps={{
          style: {
            maxHeight: 48 * 4.5,
            width: '20ch',
          },
        }}
      >
        {MenuContent}
      </Menu>
    </React.Fragment>
  );

  return (
    <Box className={classes.container} id="WidgetContainer">
      <Box display="flex" flexDirection="row" alignItems="center">
        <Typography className={classes.title}>{title}</Typography>
        {loading && (
          <Box ml={3}>
            <CircularProgress color="secondary" size={30} />
          </Box>
        )}
      </Box>

      <Divider className={classes.customDivider} />
      <Card
        className={clsx(cardStyle, classes.card, {
          [classes.padding]: padding,
        })}
      >
        {MenuContent && renderMenu()}

        {children}
      </Card>
    </Box>
  );
};

export default WidgetContainer;
