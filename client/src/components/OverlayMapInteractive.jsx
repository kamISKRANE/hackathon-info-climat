import React from 'react';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles({
  overlay: {
    position: 'absolute',
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,0.8)',
    zIndex: 1401
  },
  loader: {
    zIndex: 1402,
    position: 'absolute',
    width: '100px',
    height: '100px',
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
    margin: 'auto'
  }
});

const OverlayMapInteractive = () => {
  const classes = useStyles();

  return (
    <div className={classes.overlay}>
      <img src="/static/loader.gif" alt="Loader" className={classes.loader} />
    </div>
  );
};

export default OverlayMapInteractive;
