import HomeIcon from '@material-ui/icons/Home';
import RoomIcon from '@material-ui/icons/Room';
import PublicIcon from '@material-ui/icons/Public';
import ChatBubbleOutlineIcon from '@material-ui/icons/ChatBubbleOutline';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';

export default [
  {
    items: [
      {
        title: 'Home',
        href: '/',
        icon: HomeIcon,
      },
      {
        title: 'Map interactive',
        href: '/historic',
        icon: RoomIcon,
      },
      {
        title: 'Impact sur le climat',
        href: '/impact',
        icon: PublicIcon,
      },
      {
        title: "Paroles d'experts",
        href: '/parole',
        icon: ChatBubbleOutlineIcon,
      },

      {
        title: 'Mon agenda',
        href: '/agenda',
        icon: CalendarTodayIcon,
      },
    ],
  },
];
