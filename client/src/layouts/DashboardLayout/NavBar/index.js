/* eslint-disable operator-linebreak */
/* eslint-disable react/no-unescaped-entities */
/* eslint-disable no-use-before-define */
import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Drawer, Hidden, makeStyles } from '@material-ui/core';

import clsx from 'clsx';
import NavContent from './Nav';
import FiltersContent from './Filters';

const useStyles = makeStyles(() => ({
  mobileDrawer: {
    width: 256,
  },
  desktopDrawer: {
    width: 256,
    top: 64,
    height: 'calc(100% - 64px)',
  },

  desktopDrawerMap: {
    width: 356,
  },
  avatar: {
    cursor: 'pointer',
    width: 64,
    height: 64,
  },
}));

const NavBar = ({ onMobileClose, openMobile }) => {
  const classes = useStyles();
  const location = useLocation();

  const [navType, setNavType] = useState('MENU');

  useEffect(() => {
    if (openMobile && onMobileClose) {
      onMobileClose();
    }

    handleNavbarType(location.pathname === '/historic' ? 'FILTERS' : 'MENU');
  }, [location.pathname]);

  /**
   * @param {'MENU' | 'FILTERS'} type
   */
  const handleNavbarType = (type) => {
    setNavType(type);
  };

  const content =
    navType === 'MENU' ? (
      <NavContent />
    ) : (
      <FiltersContent backToMenu={() => handleNavbarType('MENU')} />
    );

  return (
    <>
      <Hidden lgUp>
        <Drawer
          anchor="left"
          classes={{ paper: classes.mobileDrawer }}
          onClose={onMobileClose}
          open={openMobile}
          variant="temporary"
        >
          {content}
        </Drawer>
      </Hidden>
      <Hidden mdDown>
        <Drawer
          anchor="left"
          classes={{
            paper: clsx(
              { [classes.desktopDrawerMap]: location.pathname === '/historic' },
              classes.desktopDrawer
            ),
          }}
          open
          variant="persistent"
        >
          {content}
        </Drawer>
      </Hidden>
    </>
  );
};

NavBar.propTypes = {
  onMobileClose: PropTypes.func,
  openMobile: PropTypes.bool,
};

export default NavBar;
