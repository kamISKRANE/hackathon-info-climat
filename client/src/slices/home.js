import { createSlice } from '@reduxjs/toolkit';
import axios from 'src/utils/axios';

const initialState = {
  thisDayEvents: [],
  loading: false,
};

const slice = createSlice({
  name: 'home',
  initialState,
  reducers: {
    getThisDayEvents(state, action) {
      state.thisDayEvents = action.payload;
      state.loading = false;
    },
    loaderStart(state) {
      state.loading = true;
    },
  },
});

export const { reducer } = slice;

export const getThisDayEvents = () => async (dispatch) => {
  dispatch(slice.actions.loaderStart());

  // TODO:
  const url = '/historic-events/31';
  const response = await axios.get(url);

  dispatch(slice.actions.getThisDayEvents(response.data));
};
