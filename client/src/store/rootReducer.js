import { combineReducers } from '@reduxjs/toolkit';

import { reducer as mapReducer } from 'src/slices/map';
import { reducer as homeReducer } from 'src/slices/home';

const rootReducer = combineReducers({
  map: mapReducer,
  home: homeReducer,
});

export default rootReducer;
