import {
  useDispatch as useReduxDispatch,
  useSelector as useReduxSelector,
} from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';
import { ENABLE_REDUX_DEV_TOOLS } from 'src/constants/app';
import rootReducer from './rootReducer';

const currentState = localStorage.getItem('reduxState');
const persistedState = currentState ? JSON.parse(currentState) : {};

const store = configureStore({
  reducer: rootReducer,
  devTools: ENABLE_REDUX_DEV_TOOLS,
  preloadedState: persistedState,
});

store.subscribe(() => {
  localStorage.setItem(
    'reduxState',
    JSON.stringify({
      home: store.getState().home,
    })
  );
});

export const useSelector = useReduxSelector;

export const useDispatch = () => useReduxDispatch();

export default store;
