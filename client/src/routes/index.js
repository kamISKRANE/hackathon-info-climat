/* eslint-disable import/no-unresolved */
import React, { Suspense, Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';

import LoadingScreen from 'src/components/LoadingScreen';

// eslint-disable-next-line no-shadow
const renderRoutes = (routes = []) => (
  <Suspense fallback={<LoadingScreen />}>
    <Switch>
      {routes.map((route, i) => {
        const Layout = route.layout || Fragment;
        const Component = route.component;

        return (
          <Route
            key={i}
            path={route.path}
            exact={route.exact}
            render={(props) => (
              <Layout>
                {route.routes ? (
                  renderRoutes(route.routes)
                ) : (
                  <Component {...props} />
                )}
              </Layout>
            )}
          />
        );
      })}
    </Switch>
  </Suspense>
);

export default renderRoutes;
