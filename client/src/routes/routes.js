/* eslint-disable import/no-unresolved */
import React, { lazy } from 'react';

import { Redirect } from 'react-router-dom';
import DashboardLayout from 'src/layouts/DashboardLayout';

export default [
  {
    path: '/',
    layout: DashboardLayout,
    routes: [
      {
        exact: true,
        path: '/',
        component: lazy(() => import('src/views/HomeView')),
      },
      {
        exact: true,
        path: '/historic',
        component: lazy(() => import('src/views/HistoricView')),
      },

      {
        exact: true,
        path: '/widget-grid',
        component: lazy(() => import('src/views/WidgetConfig')),
      },

      {
        component: () => <Redirect to="/404" />,
      },
    ],
  },
];
