import { useContext } from 'react';

import SettingsContext from 'src/contexts/settings.context';

const useSettings = () => useContext(SettingsContext);

export default useSettings;
