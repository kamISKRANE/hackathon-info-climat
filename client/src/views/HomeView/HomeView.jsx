import { Box, Container } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import RGL, { WidthProvider } from 'react-grid-layout';

import { getComponent } from 'src/components/widgetConfig/gridConfig';

import DefaultDashboard from './DefaultDashboard';

const ResponsiveReactGridLayout = WidthProvider(RGL);

const HomeView = () => {
  const [gridLayout, setGridLayout] = useState(undefined);

  useEffect(() => {
    const grid = localStorage.getItem('widget-layout');

    if (grid) setGridLayout(JSON.parse(grid));
  }, []);

  const generateGrid = () =>
    gridLayout.map((elm, idx) => {
      const Component = getComponent(elm.i);
      return (
        <div key={idx} data-grid={elm}>
          <Component />
        </div>
      );
    });

  return (
    <Container maxWidth={false}>
      <Box pt={5}>
        {gridLayout ? (
          <ResponsiveReactGridLayout
            isDraggable={false}
            isResizable={false}
            cols={12}
          >
            {generateGrid()}
          </ResponsiveReactGridLayout>
        ) : (
          <DefaultDashboard />
        )}
      </Box>
    </Container>
  );
};

export default HomeView;
