import { Box, Grid } from '@material-ui/core';
import React from 'react';
import {
  Webcam,
  Event,
  DidYouKnow,
  MapInteractive,
  DailyNews,
  OnThisDay,
  SocialNetwork
} from 'src/components/dashboard/widgets';

const DefaultDashboard = () => (
  <Grid container spacing={5}>
    <Grid item md={4}>
      <Box pb={5}>
        <MapInteractive />
      </Box>
      <Box pb={5}>
        <Webcam />
      </Box>
    </Grid>

    <Grid item md={3} pb={5}>
      <Box pb={5}>
        <DailyNews />
      </Box>
    </Grid>

    <Grid item md={4}>

      <Box pb={5}>
        <DidYouKnow />
      </Box>
      <Box pb={5}>
        <Event />
      </Box>
      <Box pb={5}>
        <SocialNetwork />
      </Box>
    </Grid>

    <Grid item md={9}>
      <OnThisDay />
    </Grid>
  </Grid>
);

export default DefaultDashboard;
