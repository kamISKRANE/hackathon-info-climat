/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import {
  Box,
  Container,
  makeStyles,
  Typography,
  Divider,
  FormControl,
  FormLabel,
  FormGroup,
  Checkbox,
  FormControlLabel,
  IconButton,
  SvgIcon,
  Button,
} from '@material-ui/core';
import React, { useEffect, useState } from 'react';

import RGL, { WidthProvider } from 'react-grid-layout';

import isEmpty from 'lodash/isEmpty';

import ClearIcon from '@material-ui/icons/Clear';

import {
  widgetGridConfig,
  getComponent,
} from 'src/components/widgetConfig/gridConfig';
import { useSnackbar } from 'notistack';

const ResponsiveReactGridLayout = WidthProvider(RGL);

const useStyles = makeStyles(() => ({
  root: {},

  removeBtn: {
    position: 'absolute',
    right: '2px',
    top: 0,
  },

  top: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
}));

const WidgetConfig = () => {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();

  const [state, setState] = useState({
    items: [],
    itemsName: [],
  });

  const [gridState, setGridState] = useState([]);

  useEffect(() => {
    const localGrid = localStorage.getItem('widget-layout');

    if (localGrid) {
      const data = JSON.parse(localGrid);

      setState({
        items: data,
        itemsName: data.map((w) => w.i),
      });

      setGridState(localGrid);
    }
  }, []);

  const handleRemove = (widget) => {
    setState((st) => ({
      items: st.items.filter((item) => item.i !== widget.i),
      itemsName: st.itemsName.filter((name) => name !== widget.i),
    }));
  };

  const handleSetItem = (widget) => {
    if (state.itemsName.includes(widget.i)) {
      handleRemove(widget);
      return;
    }

    setState((st) => ({
      items: [...st.items, { ...widget }],
      itemsName: [...st.itemsName, widget.i],
    }));
  };

  const resetWidgets = () => {
    setState({ items: [], itemsName: [] });
    setGridState([]);

    localStorage.removeItem('widget-layout');
  };

  const generateGrid = () =>
    state.items.map((elm) => {
      const i = elm.add ? '+' : elm.i;
      const Component = getComponent(elm.i);
      return (
        <div key={i} data-grid={elm} className={classes.root}>
          <Component />

          <IconButton
            className={classes.removeBtn}
            color="inherit"
            onClick={() => handleRemove(elm)}
          >
            <SvgIcon fontSize="small">
              <ClearIcon />
            </SvgIcon>
          </IconButton>
        </div>
      );
    });

  const onLayoutChange = (gridData) => {
    setGridState(gridData);
  };

  const handleSaveToLS = () => {
    localStorage.setItem('widget-layout', JSON.stringify(gridState));

    enqueueSnackbar('Sauvegarde effectuée   ', {
      variant: 'success',
    });
  };

  return (
    <Container maxWidth={false}>
      <Box pt={3} pb={5}>
        <Box mb={2}>
          <Typography variant="h2" component="h2">
            Configuration de widgets
          </Typography>
        </Box>

        <Box className={classes.top}>
          <FormControl component="fieldset">
            <FormLabel component="legend">Widgets disponible</FormLabel>
            <FormGroup row>
              {widgetGridConfig.map((widget, idx) => (
                <FormControlLabel
                  key={idx}
                  control={
                    <Checkbox
                      checked={state.itemsName.includes(widget.i)}
                      onChange={() => handleSetItem(widget)}
                    />
                  }
                  label={widget.label}
                />
              ))}
            </FormGroup>
          </FormControl>

          <Box display="flex" flexDirection="row">
            <Button
              variant="outlined"
              color="secondary"
              size="small"
              onClick={resetWidgets}
              disabled={isEmpty(gridState)}
            >
              Réinitialiser
            </Button>

            <Box ml={2}>
              <Button
                disabled={isEmpty(state.items)}
                size="small"
                color="secondary"
                variant="contained"
                onClick={handleSaveToLS}
              >
                Sauvegarder
              </Button>
            </Box>
          </Box>
        </Box>
        <Divider />

        <div>
          <ResponsiveReactGridLayout
            onLayoutChange={onLayoutChange}
            isDraggable
            isResizable
            cols={12}
          >
            {generateGrid()}
          </ResponsiveReactGridLayout>
        </div>
      </Box>
    </Container>
  );
};

export default WidgetConfig;
