import { DataTypes, Model } from "sequelize";

class HistoricValues extends Model {
    static init(sequelize) {
        super.init(
            {
                id_historic: {
                    type: DataTypes.INTEGER,
                    allowNull: false,
                },
                lieu: {
                    type: DataTypes.STRING,
                    allowNull: true,
                },
                geoid: {
                    type: DataTypes.INTEGER,
                    allowNull: true,
                },
                dept: {
                    type: DataTypes.STRING,
                    allowNull: true,
                },
                pays: {
                    type: DataTypes.STRING,
                    allowNull: true,
                },
                valeur: {
                    type: DataTypes.FLOAT,
                    allowNull: true,
                },
                type: {
                    type: DataTypes.INTEGER,
                    allowNull: true,
                },

                date: {
                    type: DataTypes.DATE,
                    allowNull: true,
                },

                commentaire: {
                    type: DataTypes.STRING,
                    allowNull: true,
                },

                est_record: {
                    type: DataTypes.INTEGER,
                    allowNull: true,
                },

                est_record_dpt: {
                    type: DataTypes.INTEGER,
                    allowNull: true,
                },
            },
            {
                sequelize,
                modelName: "HistoricValues",
                tableName: "historic_values",
                timestamps: false,
            }
        );
    }

    static associate(models) {}
}

export default HistoricValues;
