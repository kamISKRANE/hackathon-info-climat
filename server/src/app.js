import express from "express";
import morgan from "morgan";
import cors from "cors";
import helmet from "helmet";

import RouterManager from "./routes";

const app = express();

import("./database");
// import("./lib/schedule");

// views
app.use(express.static(__dirname + "/../public"));

// dependencies
app.use(express.json({ limit: "20mb", extended: true }));
app.use(express.urlencoded({ extended: true }));
app.use(cors());
app.use(helmet());
app.use(morgan("dev"));

// routes
RouterManager(app);

export default app;
