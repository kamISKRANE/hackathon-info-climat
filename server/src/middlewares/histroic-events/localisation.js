import control from "./control";

const LOCALISATION_MAPPING = {
    42: {
        localisation: 42,
        regionName: "Alsace",
        departments: ['67', '68'],
    },
    72: {
        localisation: 72,
        regionName: "Aquitaine",
        departments: ['24','33','40','47','64'],
    },
    83: {
        localisation: 83,
        regionName: "Auvergne",
        departments: ['15','43','63','03'],
    },
    26: {
        localisation: 26,
        regionName: "Bourgogne",
        departments: ['21','58','71','89'],
    },
    53: {
        localisation: 53,
        regionName: "Bretagne",
        departments: ['22','29','35','56'],
    },
    21: {
        localisation: 21,
        regionName: "Champagne-Ardenne",
        departments: ['10','51','52','08'],
    },
    94: {
        localisation: 94,
        regionName: "Corse",
        departments: ['2A', '2B'],
    },
    43: {
        localisation: 43,
        regionName: "Franche-Comté",
        departments: ['25','39','70','90'],
    },
    11: {
        localisation: 11,
        regionName: "Île-de-France",
        departments: ['75','91','92','93','77','94','95','78'],
    },
    91: {
        localisation: 91,
        regionName: "Languedoc-Roussillon",
        departments: ['11','30','34','48','66'],
    },
    74: {
        localisation: 74,
        regionName: "Limousin",
        departments: ['19','23','87'],
    },
    24: {
        localisation: 24,
        regionName: "Centre",
        departments: ['18','28','36','37','41','45'],
    },
    41: {
        localisation: 41,
        regionName: "Lorraine",
        departments: ['54','55','57','88'],
    },
    73: {
        localisation: 73,
        regionName: "Midi-Pyrénées",
        departments: ['12','31','32','46','65','81','82','09'],
    },
    31: {
        localisation: 31,
        regionName: "Nord-Pas-de-Calais",
        departments: ['59','62'],
    },
    25: {
        localisation: 25,
        regionName: "Basse-Normandie",
        departments: ['14','50','61'],
    },
    23: {
        localisation: 23,
        regionName: "Haute-Normandie",
        departments: ['27','76'],
    },
    52: {
        localisation: 52,
        regionName: "Pays de la Loire",
        departments: ['44','49','53','72','85'],
    },
    22: {
        localisation: 22,
        regionName: "Picardie",
        departments: ['60','80','02'],
    },
    54: {
        localisation: 54,
        regionName: "Poitou-Charentes",
        departments: ['16','17','79','86'],
    },
    93: {
        localisation: 93,
        regionName: "PACA",
        departments: ['13','83','84','04','05','06'],
    },
    82: {
        localisation: 82,
        regionName: "Rhône-Alpes",
        departments: ['26','38','42','69','73','74','01','07'],
    }
};

export default (data) => {
    const parse = (eventObj) => {
        const mappedLocalisation = LOCALISATION_MAPPING[eventObj.localisation];

        return mappedLocalisation
            ? {
                  ...eventObj,
                  localisation: mappedLocalisation,
              }
            : eventObj;
    };

    return control(data, parse);
};
