import HistoricEvents from "../models/HistoricEvents";
import { resCatchError } from "../helpers/error";
import { getByLocationWithFiltersSearch, getByLocationWithFiltersData, getMinAndMaxDate } from "../services/events.services.js";

function getOne(req, res) {
  HistoricEvents.findByPk(req.params.id)
    .then((data) => (data ? res.json(data) : res.sendStatus(404)))
    .catch((err) => resCatchError(res, err));
}

function getAll(req, res) {
  HistoricEvents.findAll({
    where: {
      ...req.query,
    },
  })
    .then((data) => (data ? res.json(data) : res.sendStatus(404)))
    .catch((err) => resCatchError(res, err));
}

async function getByLocationData(req, res) {
  getByLocationWithFiltersData(req).then((data) => (data ? res.json(data) : res.sendStatus(404)));
}

async function getByLocationSearch(req, res) {
  getByLocationWithFiltersSearch(req).then((data) => (data ? res.json(data) : res.sendStatus(404)));
}

async function getMinAndMax(req, res) {
  getMinAndMaxDate(req).then((data) => (data ? res.json(data) : res.sendStatus(404)));
}

export {getOne, getAll, getByLocationSearch, getByLocationData, getMinAndMax};
