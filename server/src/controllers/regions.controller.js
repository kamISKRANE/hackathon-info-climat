import Regions from "../models/Regions";
import { resCatchError } from "../helpers/error";

function getOne(req, res) {
    Regions.findByPk(req.params.id)
        .then((data) => (data ? res.json(data) : res.sendStatus(404)))
        .catch((err) => resCatchError(res, err));
}

function getAll(req, res) {
    Regions.findAll({
        where: {
            ...req.query,
        },
    })
        .then((data) => (data ? res.json(data) : res.sendStatus(404)))
        .catch((err) => resCatchError(res, err));
}

export { getOne, getAll };
