import { Router } from "express";
import { getOne, getAll, getByLocationData, getByLocationSearch, getMinAndMax } from "../controllers/historic-events.controller";

const router = Router();

router.get('/get-min-and-max-date', getMinAndMax)
router.get('/by-location-data', getByLocationData)
router.get('/by-location-search', getByLocationSearch)
router.get('/:id', getOne);
router.get('', getAll);

export default router;
