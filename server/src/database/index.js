import sequelize from "../lib/sequelize";

import HistoricEvents from "../models/HistoricEvents";
import HistoricValues from "../models/HistoricValues";
import HistoricNormales from "../models/HistoricNormales";
import Station from "../models/Station";
import ClimatologieJournaliere from "../models/ClimatologieJournaliere";

HistoricEvents.init(sequelize);
HistoricValues.init(sequelize);
Station.init(sequelize);
ClimatologieJournaliere.init(sequelize);
HistoricNormales.init(sequelize);
sequelize.sync({});
